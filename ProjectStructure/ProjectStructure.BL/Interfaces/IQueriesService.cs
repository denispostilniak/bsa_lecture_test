﻿using System;
using System.Collections.Generic;
using System.Text;
using ProjectStructure.Common.Models.DTO;
using ProjectStructure.Common.Models.StructureModels;
using ProjectStructure.DAL.Models;

namespace ProjectStructure.BL.Interfaces
{
    public interface IQueriesService
    {
        void AddUser(User user);
        void AddPoject(Project project);
        void AddTeam(Team team);
        void AddTask(Task task);
        Dictionary<string, int> GetTasksByUser(int id);
        IEnumerable<TaskDTO> GetTasksByUserNameCondition(int id);
        IEnumerable<TaskDTO> GetTasksByUserCurrentYear(int id);
        IEnumerable<TaskDTO> GetUnFinishedUserTasks(int userId);
        IEnumerable<(int, string, IEnumerable<UserDTO>)> GetTeamsOrderTenYears();
        IEnumerable<UserDTO> GetUsersSortedByNameAndTasks();
        LastProjectCountAndLongerTasks GetLastProjectCountAndLongerTasks(int userId);
        IEnumerable<ProjectLongestAndShortestTaskAndUsersAmount> GetProjectLogestAndShortestTaskAndUsersAmount();
    }
}
